# AOP 动态代理 
    
      你有a类，本来是调用c类的方法，完成某个功能，但c不让a调用。 
       a---不能调用c的方法 
       在a与c之间 创建一个b代理， c让b访问。 
       a---访问b---访问c 
     

    
#      实现代理的方式： 
      1： 静态代理： 1）代理类是自己手工实现的，自己创建一个java类，表示代理类 
                   2）同时你所要代理的目标类是确定的。 
                   特点：实现简单，容易理解。 
                   例如：卖U盘。用户（客户端）---商家（代理）---厂家（目标） 
                   1）创建一个接口，定义卖u盘的方法 
                   2）创建厂家类 
                   3）创建商家类 
                   4）创建客户端类 
            缺点：1）商家固定，如果多个商家需要添加多个代理； 
                 2）如果接口中增加一个方法，所有的代理也都要实现这个方法。 
     
     
      2： 动态代理： 在静态代理中目标类很多时候，可以使用动态代理，避免静态代理的缺点。 
                   动态代理中目标类即使很多，代理类数量可以很少，当你修改了接口中的方法时，不会影响代理类。 
             在程序执行过程中，使用jdk的反射机制，创建代理类对象，并动态的指定要代理目标类。 
             换句话说：动态代理是一种创建java对象的能力，让你不用创建TaobaoShangjia类，就能创建代理类对象。 
     
             作用：可以在不改变原来目标方法功能的前提下，可以在代理中增强自己的功能代码。 
             比如，你所在的项目中，有一个功能是其他人写好的，你可以使用，并在里面增加自己的需求。 
     
             代理的作用：控制访问和控制增强 
             动态代理有两种实现方式：使用jdk动态代理、通过CGLIB动态代理 
             1）jdk动态代理：通过java的反射包java.lang.reflect中的三个类：InvocationHandler、Method、Proxy。 
                           要求必须有接口。 
             2) cglib动态代理：cglib是第三方的工具库，创建代理对象。不需要有接口。 
                   cglib的原理是继承，cglib通过继承目标类，创建它的子类，在子类中重新父类中同名的方法，实现功能修改。 
                   因为cglib是继承，重写方法，所以要求目标类不能是final的，方法也不能是final的。 
                   cglib的要求目标类比较宽松，只要能继承就可以了，cglib在很多框架中使用，比如mybatis、spring框架中都有使用。 
     