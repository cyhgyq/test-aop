package com.cyh.dongtai;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MySellHandler implements InvocationHandler {
    private Object target = null;

    //动态传入目标
    public MySellHandler(Object target) {
        System.out.println("----000----");
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("----111----");
        Object res = null;
        //执行目标方法
        res = method.invoke(target, args);

        //增强方法
        Float price = null;
        if(res != null) {
            price = (Float)res;
            price = price + 25;
        }
        return price;
    }
}
