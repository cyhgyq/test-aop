package com.cyh.dongtai;

import com.cyh.jintai.TaobaoShangjia;
import com.cyh.jintai.TianmaoShangjia;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class Test03 {
    public static void main(String[] args) {
        //创建代理对象
        UsbSell2 factory2 = new UsbKingFactory2();
        InvocationHandler handler = new MySellHandler(factory2);
        UsbSell2 proxy = (UsbSell2) Proxy.newProxyInstance(factory2.getClass().getClassLoader(),
                factory2.getClass().getInterfaces(),
                handler);
        float price = proxy.sell(1);
        System.out.println("----333----" + price);
    }

}
