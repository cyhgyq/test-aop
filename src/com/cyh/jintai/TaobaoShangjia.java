package com.cyh.jintai;

public class TaobaoShangjia implements UsbSell{
    private UsbKingFactory factory = new UsbKingFactory();
    @Override
    public float sell(int amount) {
        float price = factory.sell(amount);
        price = price + 25;
        return price;
    }
}
