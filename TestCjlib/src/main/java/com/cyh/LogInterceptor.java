package com.cyh;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

//拦截器1
public class LogInterceptor implements MethodInterceptor {

    @Override
    public Object intercept(Object proxyInstance, Method superMethod, Object[] parameterValues, MethodProxy methodProxy) throws Throwable {
        // 打印代理子类类名
        System.out.println("obj: " + proxyInstance.getClass().getCanonicalName());
        // 打印委托基类方法名
        System.out.println("method: " + superMethod.getDeclaringClass() + "." + superMethod.getName());
        // 打印参数对象
        for (int i = 0; i < parameterValues.length; i++) {
            System.out.println("args[" + i + "]: " + parameterValues[i]);
        }
        // 调用原始方法
        Object result = methodProxy.invokeSuper(proxyInstance, parameterValues);
        // 返回调用结果
        return result;
    }
}
