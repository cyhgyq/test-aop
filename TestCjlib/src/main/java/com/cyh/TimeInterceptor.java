package com.cyh;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

//拦截器2
public class TimeInterceptor implements MethodInterceptor {

    @Override
    public Object intercept(Object proxyInstance, Method superMethod, Object[] parameterValues, MethodProxy methodProxy) throws Throwable {
        long startTime = System.nanoTime();
        // 调用原始方法
        Object result = methodProxy.invokeSuper(proxyInstance, parameterValues);
        // 打印耗时
        System.out.println(superMethod.getName() + " cost " + (System.nanoTime() - startTime) + " ns.");
        // 返回调用结果
        return result;
    }
}
