package com.cyh;

import net.sf.cglib.proxy.CallbackFilter;

import java.lang.reflect.Method;

//拦截映射
public class InterceptorFilter implements CallbackFilter {

    @Override
    public int accept(Method method) {
        String methodName = method.getName();
        if (methodName.equals("update")) {
            return 1;
        }
        return 0;
    }
}
