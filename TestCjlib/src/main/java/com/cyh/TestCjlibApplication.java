package com.cyh;

import net.sf.cglib.proxy.Callback;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.NoOp;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCjlibApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestCjlibApplication.class, args);



        //测试

        // 保存生成的代理子类Class文件
        //System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "D:\\Temp\\CGLib");

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(DelegateClass.class);
        // 多种拦截
        enhancer.setCallbacks(new Callback[]{new LogInterceptor(), new TimeInterceptor(), NoOp.INSTANCE});
        // 方法与拦截之间的映射
        enhancer.setCallbackFilter(new InterceptorFilter());
        // 构造器中调用方法不被拦截
        enhancer.setInterceptDuringConstruction(false);

        DelegateClass delegateInstance = (DelegateClass) enhancer.create();
        delegateInstance.add("Tom", 30);
        delegateInstance.update();
    }

}
